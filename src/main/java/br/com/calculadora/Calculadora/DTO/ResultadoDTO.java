package br.com.calculadora.Calculadora.DTO;

public class ResultadoDTO {

    private Integer resultado;

    public ResultadoDTO() {
    }

    public ResultadoDTO(Integer resultado) {
        this.resultado = resultado;
    }

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }
}
