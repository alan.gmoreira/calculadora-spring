package br.com.calculadora.Calculadora.Services;

import br.com.calculadora.Calculadora.DTO.ResultadoDTO;
import br.com.calculadora.Calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public ResultadoDTO somar(Calculadora calculadora)
    {
        Integer retorno = 0;
        for (Integer i : calculadora.getNumeros()
        ) {
            retorno += i.intValue();
        }
        return new ResultadoDTO(retorno);
    }

    public ResultadoDTO subtrair(Calculadora calculadora)
    {
        Integer retorno = 0;
        for (Integer i : calculadora.getNumeros()
        ) {
            if(retorno == 0)
                retorno = i.intValue();
            else
                retorno -= i.intValue();
        }
        return new ResultadoDTO(retorno);
    }

    public ResultadoDTO multiplicar(Calculadora calculadora)
    {
        Integer retorno = 1;
        for (Integer i : calculadora.getNumeros()
        ) {
            retorno *= i.intValue();
        }
        return new ResultadoDTO(retorno);
    }

    public ResultadoDTO dividir(Calculadora calculadora)
    {
        return new ResultadoDTO(calculadora.getNumeros().get(0) / calculadora.getNumeros().get((1)));
    }

}
