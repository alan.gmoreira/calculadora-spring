package br.com.calculadora.Calculadora.models;

import java.util.List;

public class Calculadora {
    List<Integer> numeros;

    public Calculadora() {
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
