package br.com.calculadora.Calculadora.controllers;

import br.com.calculadora.Calculadora.DTO.ResultadoDTO;
import br.com.calculadora.Calculadora.Services.CalculadoraService;
import br.com.calculadora.Calculadora.models.Calculadora;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheAnnotationParser;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("somar")
    public ResultadoDTO somar(@RequestBody Calculadora calculadora) {

        if (calculadora.getNumeros().size() > 1)
            return calculadoraService.somar(calculadora);
        else
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessario passar pelo menos 2 numeros para serem somados.");
    }

    @PostMapping("subtrair")
    public ResultadoDTO subtrair(@RequestBody Calculadora calculadora) {

        if (calculadora.getNumeros().size() > 1)
            return calculadoraService.subtrair(calculadora);
        else
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessario passar pelo menos 2 numeros para serem subtraidos.");
    }

    @PostMapping("multiplicar")
    public ResultadoDTO multiplicar(@RequestBody Calculadora calculadora) {

        if (calculadora.getNumeros().size() > 1)
            return calculadoraService.multiplicar(calculadora);
        else
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessario passar pelo menos 2 numeros para serem multiplicados.");
    }

    @PostMapping("dividir")
    public ResultadoDTO dividir(@RequestBody Calculadora calculadora) {

        if (calculadora.getNumeros().size() == 2) {
            if (calculadora.getNumeros().get(0) > calculadora.getNumeros().get(1)) {
                if (calculadora.getNumeros().get(1) == 0) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é possivel fazer divisões por ZERO.");
                } else
                    return calculadoraService.dividir(calculadora);
            } else
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O primeiro numero precisa ser maior que o segundo numero.");
        }else
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessario enviar exatamente dois numeros para fazer divisão.");
    }

}
